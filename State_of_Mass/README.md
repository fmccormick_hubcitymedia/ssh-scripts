Written by fmccormick@hubcitymedia.com

If you are unable to run this script because it does not know what paramiko is, run this command in your console:

    sudo pip install paramiko

If you are unable to run THAT command because it does not know what pip is, run this command in your console:

    sudo easy_install pip

If you are unable to run this script because it does not know what argparse is, run this command in your console:

    sudo pip install argparse

-------------------------------------------------------------

This module and its scripts were written by Frank McCormick of Hub City Media for the Managed Support Services team.

The goal of this project was to automate the process of:

1. logging on to a variable number of machines individually based on the contents of your local properties file,
2. uploading a log-filtering mini-script to that machine in the user's home directory,
3. running that mini-script to strip the logs of unnecessary message and seriously cut down on the data necessary for transfer,
4. reading the standard output of the remote machine and piping it to the local machine executing this script,
5. parsing the important log messages and comparing them against a database/csv for their descriptions and recommended actions to take for them, as well as how severe they are

-------------------------------------------------------------

## STEPS TO RUN:

1. cd to your local copy of my git repo
2. execute the following command:
    -   python mss.py -u [username] -p [password] -i all_service_errors.csv (-a to trust new servers, if you've never connected to them with this script before)
3. sit back, relax, light some candles
    -   script will sequentially connect to, parse logs, and disconnect from every server listed in mss.py
    -   Stabilizing pipe will cause the longest latency, however it should not be more than a minute or two.  If it is, that means that something on the remote server has crashed
4. Once all lines are parsed and processed, output will be given in the following format:
    -   ALERTS
    -   BENIGNS
    -   UNKNOWN (not given a severity level)
    -   NEW (never-before-documented)
        *   Stacktraces are ON by default, but can be turned off via command line with the ‘-s’ option
        *   Errors that are ONLY stack traces are permanently set to ON
        *   You can watch the output as it scrolls by, or just read it all in one go once the script has finished.
5. Make note of any ALERTS that need to be handled or investigated
6. Attempt to set the severity of any UNKNOWN errors, do some investigating/researching
7. Document the full error and unique regex identifier of all NEW errors in the csv file
8. Send the email out addressed to Naveen with any information you need to give

-------------------------------------------------------------

## TROUBLESHOOTING:

**Why is 'preparing pipe' taking so long?**

        -   The console displays 'preparing pipe' when it is setting up the avenue for data transmission between your machine and the machine you have connected to.
        -   This is the slowest part of the script, but even with a busy remote machine and poor internet connection, should not take longer than maybe several minutes at absolute most.
            *   Use your best judgment here
        -   If you need to be on a VPN to access the machine you are trying to access, and you disconnect from the VPN while preparing the pipe, the program will hang indefinitely.
            *   This is an unfortunate but very unlikely situation, and the only solution is to stop the script, reconnect to VPN, and start the script again.

**Under what circumstances do I need to restart the script?**

        -   As already previously mentioned, if your script needs access to a VPN and you lose your connection to the VPN during its run, you must reconnect and restart.
        -   I am not presently aware of any other circumstances that would cause you to need to restart.

**How do I make the script access servers in a different order, or skip a few at the start?**

        -   The script connects to servers in the order that they are listed in the properties file
        -   If you would like to start at, say, the 5th server, because that was the one that crashed on your last run,
            you just need to comment out or remove the first 4 servers' blocks from the properties file
        -   If you would like to connect in a different order, just reorder the properties file the way you would like

**How do I use the properties file?**

    The properties file is what the script uses as information needed for the machine you are connecting to.

    The required parameters are:
        -   service         (The acronym for the oracle service hosted on this service, i.e. OHS, OAM, OVD, etc.)
        -   hostname        (The address of the machine you are trying to connect to)
        -   log_directory   (The directory on the machine that holds the logs, ending in a '/')
        -   log_prefix      (The text that prefixes every single log file, i.e. 'oim-diagnostic')

    The optional parameters are:
        -   process         (May have multiple lines for process.  You may supply a partial match for the name of a process to check,
                            and a float as an acceptable threshold for that process' memory usage.  These two values are separated by a ',')
        -   superuser       (The user you wish to masquerade as when sudo-executing commands.  Defaults to root.)

    The "comment character" is '#', just like in python

    Here is an example block from a working properties file:

        # OHS1
        service:OHS
        hostname:orion-mao-prodohs1vg
        log_directory:/opt/AIMS/Oracle/Middleware/Oracle_WT1/instances/instance1/diagnostics/logs/OHS/ohs1/
        log_prefix:access_log
        process:-Dweblogic.Name=AdminServer,5.0
        superuser:aimsadmin

