# Project Name: HCM Managed Support Services
# File Name: oracle_error.py
# This file creates the object class for the errors discovered in oracle logs
# This file is meant to be imported by ssh.py in order to best store the parsed log information


class OracleError():
    def __init__(self, identifier, description=None, action=None, occurrences=0, timestamp_header=None, timestamps='', stacktrace='', caused_by='', status='new', is_regex_compiled=None, compiled_pattern=None, full_message=None, line_number=None):
        self.identifier = identifier
        self.description = description
        self.action = action
        self.occurrences = occurrences
        self.timestamp_header = timestamp_header
        self.timestamps = timestamps
        self.stacktrace = stacktrace
        self.caused_by = caused_by
        self.status = status
        self.is_regex_compiled = is_regex_compiled
        self.compiled_pattern = compiled_pattern
        self.full_message = full_message
        self.line_number = line_number

    def wipe(self):
        self.occurrences = 0
        self.timestamp_header = None
        self.timestamps = ''
        self.stacktrace = ''
        self.caused_by = ''
        self.is_regex_compiled = None
        self.compiled_pattern = None
        self.full_message = None
        self.line_number = None