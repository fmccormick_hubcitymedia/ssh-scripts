# Project Name: HCM Managed Support Services
# File Name: send_mail.py
# This file creates the functionality for sending emails
# This file is meant to be imported by any script that needs to send emails

import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email.MIMEText import MIMEText
from email import Encoders
import os

gmail_user = "fmccormick@hubcitymedia.com"
gmail_pwd = ""


class Email():
    def __init__(self, password=None, from_address=None, to_address=None, subject=None, body=None, attachment=None):
        self.object = MIMEMultipart()
        self.object['From'] = from_address
        if isinstance(to_address, list):
            self.object['To'] = ", ".join(to_address)
            self.to = to_address
        else:
            self.object['To'] = to_address
            self.to = to_address
        self.object['Subject'] = subject
        self.password = password
        if body:
            self.object.attach(MIMEText(body, 'html'))
        if attachment:
            part = MIMEBase('application', 'octet-stream')
            part.set_payload(open(attachment, 'rb').read())
            Encoders.encode_base64(part)
            part.add_header('Content-Disposition', 'attachment; filename="%s"' % os.path.basename(attachment))
            self.object.attach(part)

    def set_from(self, address):
        self.object['From'] = address

    def set_to(self, address):
        self.object['To'] = address
        self.to = address

    def set_array_to(self, address_array):
        self.object['To'] = ", ".join(address_array)
        self.to = address_array

    def set_subject(self, text):
        self.object['Subject'] = text

    def set_body(self, text):
        self.object.attach(MIMEText(text))

    def attach(self, attachment):
        part = MIMEBase('application', 'octet-stream')
        part.set_payload(open(attachment, 'rb').read())
        Encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment; filename="%s"' % os.path.basename(attachment))
        self.object.attach(part)

    def send_mail(self):
        mailServer = smtplib.SMTP("smtp.gmail.com", 587)
        mailServer.ehlo()
        mailServer.starttls()
        mailServer.ehlo()
        mailServer.login(self.object['From'], self.password)
        mailServer.sendmail(self.object['From'], self.to, self.object.as_string())
        # Should be mailServer.quit(), but that crashes...
        mailServer.close()





def test(to, subject, text, attach):
    msg = MIMEMultipart()

    msg['From'] = gmail_user
    msg['To'] = to
    msg['Subject'] = subject

    msg.attach(MIMEText(text))

    part = MIMEBase('application', 'octet-stream')
    part.set_payload(open(attach, 'rb').read())
    Encoders.encode_base64(part)
    part.add_header('Content-Disposition', 'attachment; filename="%s"' % os.path.basename(attach))
    msg.attach(part)

    mailServer = smtplib.SMTP("smtp.gmail.com", 587)
    mailServer.ehlo()
    mailServer.starttls()
    mailServer.ehlo()
    mailServer.login(gmail_user, gmail_pwd)
    mailServer.sendmail(gmail_user, to, msg.as_string())
    # Should be mailServer.quit(), but that crashes...
    mailServer.close()