# Project Name: HCM Managed Support Services
# File Name: ohs.py
# This file sets up the class for a Oracle HTTP Server service connection
# This file is used by any script that wants to make a connection to a server with a Oracle HTTP Server service

__author__ = 'Frank McCormick'

from ..ssh import *
import subprocess


class OHSConnection(SSHConnection):
    # this class's self.command must be set in this __init__
    def __init__(self, flags, hostname, datafile, diagfile, logdir, logpre, memchecks, superuser):
        SSHConnection.__init__(self, flags, hostname, datafile, diagfile, logdir, logpre, memchecks, superuser)
        self.command = "python /home/" + self.username + "/filter_oracle_logs.py"
        self.service = 'OHS'
        self.read_csv(self.service)

    def get_logs(self):
        if self.days > 0:
            self.logs.append(self.log_directory + self.log_prefix)  # current day's log will always be this
        if self.days > 1:
            self.sudo_execute('ls ' + self.log_directory, user="aimsadmin")
            highest = 0
            for logfile in self.output.split():
                if self.log_prefix in logfile and logfile != self.log_prefix:
                    auto_incr_num = int(logfile.split('.')[1])
                    if auto_incr_num > highest:
                        highest = auto_incr_num
            i = 1
            while i < self.days:
                self.logs.append(self.log_directory + self.log_prefix + '.' + str(highest - ((i -1) * 86400)))  # there are 86,400 seconds in a day, which is how the logs are divided
                i += 1

    def get_weekly_logs(self):
        local_log_directory = "filtered/" + self.hostname
        self.logs.append(local_log_directory + "/" + self.log_prefix)

        proc = subprocess.Popen(["ls " + local_log_directory], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        output, error = proc.communicate()

        highest = 0
        second_highest = 0
        third_highest = 0
        fourth_highest = 0
        fifth_highest = 0
        sixth_highest = 0
        for logfile in output.splitlines():
            if self.log_prefix in logfile and logfile != self.log_prefix:
                auto_incr_num = int(logfile.split('.')[1])
                if auto_incr_num > highest:
                    sixth_highest = fifth_highest
                    fifth_highest = fourth_highest
                    fourth_highest = third_highest
                    third_highest = second_highest
                    second_highest = highest
                    highest = auto_incr_num
        self.logs.append(local_log_directory + "/" + self.log_prefix + '.' + str(highest))
        self.logs.append(local_log_directory + "/" + self.log_prefix + '.' + str(second_highest))
        self.logs.append(local_log_directory + "/" + self.log_prefix + '.' + str(third_highest))
        self.logs.append(local_log_directory + "/" + self.log_prefix + '.' + str(fourth_highest))
        self.logs.append(local_log_directory + "/" + self.log_prefix + '.' + str(fifth_highest))
        self.logs.append(local_log_directory + "/" + self.log_prefix + '.' + str(sixth_highest))

    # run should perform all necessary actions for this connection
    def run(self):
        print(Bcolors.HEADER + "It is normal behavior for OHS to return few to no errors." + Bcolors.ENDC)
        self.get_logs()
        self.check_logs()
        self.check_mem()

    def run_weekly(self):
        self.get_weekly_logs()
        self.check_weekly_logs()