# Project Name: HCM Managed Support Services
# File Name: oam.py
# This file sets up the class for a Oracle Access Manager service connection
# This file is used by any script that wants to make a connection to a server with a Oracle Access Manager service

__author__ = 'Frank McCormick'

from ..ssh import *
import subprocess


class OAMConnection(SSHConnection):
    # this class's self.command must be set in this __init__
    def __init__(self, flags, hostname, datafile, diagfile, logdir, logpre, memchecks, superuser):
        SSHConnection.__init__(self, flags, hostname, datafile, diagfile, logdir, logpre, memchecks, superuser)
        self.command = "python /home/" + self.username + "/filter_oracle_logs.py"
        self.service = 'OAM'
        self.read_csv(self.service)

    def get_logs(self):
        if self.days > 0:
            self.logs.append(self.log_directory + self.log_prefix + '.log')  # current day's log will always be this
        if self.days > 1:
            self.sudo_execute('ls ' + self.log_directory, user="aimsadmin")
            highest = 0
            log_prefix = self.log_prefix + '-'
            for logfile in self.output.splitlines():
                if log_prefix in logfile and logfile != self.log_prefix:
                    auto_incr_num = int(logfile.split('.')[0].split('-')[-1])
                    if auto_incr_num > highest:
                        highest = auto_incr_num
            i = 1
            while i < self.days:
                self.logs.append(self.log_directory + log_prefix + str(highest - (i - 1)) + '.log')
                i += 1

    def get_weekly_logs(self):
        local_log_directory = "filtered/" + self.hostname
        self.logs.append(local_log_directory + "/" + self.log_prefix + ".log")

        proc = subprocess.Popen(["ls " + local_log_directory], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        output, error = proc.communicate()

        highest = 0
        log_prefix = self.log_prefix + '-'
        for logfile in output.splitlines():
            if log_prefix in logfile and logfile != self.log_prefix:
                auto_incr_num = int(logfile.split('.')[0].split('-')[-1])
                if auto_incr_num > highest:
                    highest = auto_incr_num
        self.logs.append(local_log_directory + "/" + log_prefix + str(highest) + ".log")
        self.logs.append(local_log_directory + "/" + log_prefix + str(highest - 1) + ".log")
        self.logs.append(local_log_directory + "/" + log_prefix + str(highest - 2) + ".log")
        self.logs.append(local_log_directory + "/" + log_prefix + str(highest - 3) + ".log")
        self.logs.append(local_log_directory + "/" + log_prefix + str(highest - 4) + ".log")
        self.logs.append(local_log_directory + "/" + log_prefix + str(highest - 5) + ".log")

    # run should perform all necessary actions for this connection
    def run(self):
        self.get_logs()
        self.check_logs()
        self.check_mem()

    def run_weekly(self):
        self.get_weekly_logs()
        self.check_weekly_logs()