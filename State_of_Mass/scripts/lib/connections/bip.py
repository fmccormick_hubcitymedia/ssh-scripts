# Project Name: HCM Managed Support Services
# File Name: bip.py
# This file sets up the class for a BI Publisher service connection
#       It has a unique override for check_logs() because it only has 3 log files that never change
# This file is used by any script that wants to make a connection to a server with a BI Publisher service

__author__ = 'Frank McCormick'

from ..ssh import *


class BIPConnection(SSHConnection):
    # this class's self.command must be set in this __init__
    def __init__(self, flags, hostname, datafile, diagfile, logdir, logpre, memchecks, superuser):
        SSHConnection.__init__(self, flags, hostname, datafile, diagfile, logdir, logpre, memchecks, superuser)
        self.access_log = self.log_directory + 'access.log'
        self.server_log = self.log_directory + 'bi_server1.log'
        self.diagnostic_log = self.log_directory + 'bi_server1-diagnostic.log'
        self.command = "python /home/" + self.username + "/filter_oracle_logs.py"
        self.service = 'BIP'
        self.read_csv(self.service)

    # this function puts together the array of ignorable errors and sets up the logs to be parsed
    def check_logs(self):
        self.write_filter_script_to_remote()
        if not self.blacklist:
            ignore = ""
        else:
            ignore = " | grep -v '"
            for error in self.blacklist:
                ignore = ignore + error + '\|'
            ignore = ignore[:-2] + "'"  # chop off the last unnecessary '\|'

        if self.command is None:
            print("The child class's self.command variable has not been set, so nothing can be executed")
        else:
            self.log(self.datafile, True, "=====   ACCESS   ===== - " + self.access_log)
            if not self.access_log == '':
                command = self.command + " " + self.access_log + ignore
                self.sudo_execute(command, user="aimsadmin")
                with open("filtered/" + self.hostname + "/access.log", 'w') as log:
                    log.write(self.output)
                self.parse_errors(self.output)
            else:
                msg = "\nCOULD NOT FIND ACCESS LOG, PLEASE INVESTIGATE IMMEDIATELY.\n"
                self.diagnostic_logger.error(msg)
                self.log(self.datafile, True, msg)

            self.log(self.datafile, True, "=====   SERVER   ===== - " + self.server_log)
            if not self.server_log == '':
                command = self.command + " " + self.server_log + ignore
                self.sudo_execute(command, user="aimsadmin")
                with open("filtered/" + self.hostname + "/bi_server1.log", 'w') as log:
                    log.write(self.output)
                self.parse_errors(self.output)
            else:
                msg = "\nCOULD NOT FIND ACCESS LOG, PLEASE INVESTIGATE IMMEDIATELY.\n"
                self.diagnostic_logger.error(msg)
                self.log(self.datafile, True, msg)

            self.log(self.datafile, True, "=====   DIAGNOSTIC   ===== - " + self.diagnostic_log)
            if not self.diagnostic_log == '':
                command = self.command + " " + self.diagnostic_log + ignore
                self.sudo_execute(command, user="aimsadmin")
                with open("filtered/" + self.hostname + "/bi_server1-diagnostic.log", 'w') as log:
                    log.write(self.output)
                self.parse_errors(self.output)
            else:
                msg = "\nCOULD NOT FIND ACCESS LOG, PLEASE INVESTIGATE IMMEDIATELY.\n"
                self.diagnostic_logger.error(msg)
                self.log(self.datafile, True, msg)

    def get_weekly_logs(self):
        local_log_directory = "filtered/" + self.hostname
        self.logs.append(local_log_directory + "/access.log")
        self.logs.append(local_log_directory + "/bi_server1.log")
        self.logs.append(local_log_directory + "/bi_server1-diagnostic.log")

    # run should perform all necessary actions for this connection
    def run(self):
        self.check_logs()
        self.check_mem()

    def run_weekly(self):
        self.get_weekly_logs()
        self.check_weekly_logs()