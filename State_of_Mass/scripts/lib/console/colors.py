# Project Name: HCM Managed Support Services
# File Name: colors.py
# This file provides the functionality for customizing the colors of text displayed in the terminal


class Bcolors:
    def __init__(self):
        pass

    WHITE_BG_HIGH = '\033[107m'
    CYAN_BG_HIGH = '\033[106m'
    HEADER_BG_HIGH = '\033[105m'
    BLUE_BG_HIGH = '\033[104m'
    YELLOW_BG_HIGH = '\033[103m'
    GREEN_BG_HIGH = '\033[102m'
    RED_BG_HIGH = '\033[101m'
    BLACK_BG_HIGH = '\033[100m'

    WHITE_HIGH = '\033[97m'
    CYAN_HIGH = '\033[96m'
    HEADER_HIGH = '\033[95m'
    BLUE_HIGH = '\033[94m'
    YELLOW_HIGH = '\033[93m'
    GREEN_HIGH = '\033[92m'
    RED_HIGH = '\033[91m'
    BLACK_HIGH = '\033[90m'

    WHITE_BG = '\033[47m'
    CYAN_BG = '\033[46m'
    HEADER_BG = '\033[45m'
    BLUE_BG = '\033[44m'
    YELLOW_BG = '\033[43m'
    GREEN_BG = '\033[42m'
    RED_BG = '\033[41m'
    BLACK_BG = '\033[40m'

    WHITE = '\033[37m'
    CYAN = '\033[36m'
    HEADER = '\033[35m'
    BLUE = '\033[34m'
    YELLOW = '\033[33m'
    GREEN = '\033[32m'
    RED = '\033[31m'
    BLACK = '\033[30m'

    ENDC = '\033[0m'
    DEFAULT = '\033[39m'
    DEFAULT_BG = '\033[49m'

    BLINK_SLOW = '\033[5m'
    BLINK_FAST = '\033[6m'
    BLINK_OFF = '\033[25m'


def test_console_colors():
    print(Bcolors.BLACK + ' T ' + Bcolors.RED + ' E ' + Bcolors.YELLOW + ' S ' + Bcolors.BLUE + ' T ' + Bcolors.GREEN + ' I ' + Bcolors.HEADER + ' N ' + Bcolors.CYAN + ' G ' + Bcolors.ENDC)
    print(Bcolors.BLACK_BG + ' T ' + Bcolors.RED_BG + ' E ' + Bcolors.YELLOW_BG + ' S ' + Bcolors.BLUE_BG + ' T ' + Bcolors.GREEN_BG + ' I ' + Bcolors.HEADER_BG + ' N ' + Bcolors.CYAN_BG + ' G ' + Bcolors.DEFAULT_BG)
    print(Bcolors.BLACK_HIGH + ' T ' + Bcolors.RED_HIGH + ' E ' + Bcolors.YELLOW_HIGH + ' S ' + Bcolors.BLUE_HIGH + ' T ' + Bcolors.GREEN_HIGH + ' I ' + Bcolors.HEADER_HIGH + ' N ' + Bcolors.CYAN_HIGH + ' G ' + Bcolors.ENDC)
    print(Bcolors.BLACK_BG_HIGH + ' T ' + Bcolors.RED_BG_HIGH + ' E ' + Bcolors.YELLOW_BG_HIGH + ' S ' + Bcolors.BLUE_BG_HIGH + ' T ' + Bcolors.GREEN_BG_HIGH + ' I ' + Bcolors.HEADER_BG_HIGH + ' N ' + Bcolors.CYAN_BG_HIGH + ' G ' + Bcolors.DEFAULT_BG)
    print(Bcolors.BLINK_SLOW + ' SLOW ' + Bcolors.BLINK_FAST + ' FAST ' + Bcolors.BLINK_OFF + ' OFF ')