# Project Name: HCM Managed Support Services
# File Name: ssh.py
# This is the core file of the entire HCM Managed Support Services ssh script module
#       It contains most of the functions that any other file in the project will reference
# This file is used for:
#       connecting and disconnecting from servers
#       executing commands remotely
#       executing sudo commands remotely
#       checking server disk space and memory usage metrics
#       writing filtering scripts to the remote server
#       filtering logs remotely
#       piping the data back to your local machine executing this code
#       parsing the data you received
#       comparing it against a data store, like a csv, of known log messages
#       saving results to a txt file, an html file, and color-coded printing it out to console

__author__ = 'Frank McCormick'

import paramiko  # this is the library for connecting to remote servers via ssh
import socket
import logging
import sys
import time
import re
import csv
import os
from oracle_error import *
from console.colors import *


class SSHConnection():
    # hostname = address of host to connect to
    # user = username on host
    # pwd = password for the aforementioned username on host
    # autoadd = boolean to check whether or not user is okay with automatically trusting the host they've supplied
    # command = the command to run the appropriate script for this given connection.  the parameter of which log to
    # parse will be provided at a later time
    def __init__(self, flags, hostname, datafile, diagfile, logdir, logpre, memchecks, superuser):
        if not os.path.exists("logs"):
            os.makedirs("logs")
        try:
            f = open(datafile, "r")
        except:
            f = open(datafile, "w")

        try:
            f = open(diagfile, "r")
        except:
            f = open(diagfile, "w")

        self.diagnostic_logger = logging.getLogger(diagfile)  # create logger
        self.diagnostic_logger.setLevel(logging.DEBUG)  # set minimum reporting level to debug

        # each line will start with timestamp | logger name | level of log message : Line number : log message
        formatter = logging.Formatter('%(asctime)s | {%(name)s} | [%(levelname)s] : Line %(lineno)d : %(message)s')

        fh = logging.FileHandler(filename=diagfile, mode='a')  # create file handler, tie it to the assigned diagfile
        # and set edit mode to append
        fh.setFormatter(formatter)  # add our custom format to fh
        self.diagnostic_logger.addHandler(fh)  # add fh to the logger

        self.ssh = None
        self.connected = None
        self.connection_error = None
        self.channel = None
        self.stdin = None
        self.stdout = None
        self.stderr = None
        self.output = None
        self.hostname = hostname  # host address
        self.username = flags[0]  # username
        self.password = flags[1]  # password
        self.infile = flags[2]
        self.outfile = flags[3]
        self.htmlfile = flags[4]
        self.days = int(flags[5])
        self.display_stacktrace = flags[6]
        self.display_timestamps = flags[7]
        self.autoadd = flags[8]  # whether or not to automatically trust new hosts
        self.concise = flags[9]
        self.datafile = datafile  # file to save metrics data to
        self.diagfile = diagfile  # file to save diagnostics and errors to
        self.memchecks = memchecks  # list of processes to check the memory of
        self.log_directory = logdir
        self.log_prefix = logpre
        self.superuser = superuser
        self.command = None
        self.todays_log = ''
        self.yesterdays_log = ''
        self.sundays_log = ''
        self.saturdays_log = ''  # only applicable when this script is run on a Monday
        self.fridays_log = ''  # only applicable when this script is run on a Monday
        self.thursdays_log = ''
        self.wednesdays_log = ''
        self.tuesdays_log = ''
        self.mondays_log = ''
        self.logs = []
        self.blacklist = []  # list of errors safe to ignore.  will be separated by '\|' for the grep parameters
        self.service = ''
        self.filter_code = ''
        self.documented_errors = []
        self.prefix = Bcolors.CYAN_BG
        i = 0
        while i < len(self.hostname):
            i += 1
            self.prefix += "="
        self.prefix += '\n' + self.hostname + '\n'
        i = 0
        while i < len(self.hostname):
            i += 1
            self.prefix += "="
        self.prefix += Bcolors.DEFAULT_BG
        self.suffix = ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"

        if not os.path.exists("filtered/" + self.hostname):
            os.makedirs("filtered/" + self.hostname)

    # attempts to connect to the host via ssh.  if this fails, the reason is stored in self.connection_error
    def connect(self):
        self.log(self.datafile, True, "\n" + self.prefix)
        if "prod" in self.hostname:
            print(Bcolors.HEADER + "Connecting to prod servers can take at least 15 seconds." + Bcolors.ENDC)
        sys.stdout.write("\rConnecting...")
        sys.stdout.flush()

        self.ssh = paramiko.SSHClient()  # invoke new ssh client
        paramiko.util.log_to_file("logs/paramiko.log")
        self.ssh.load_system_host_keys()  # loads your local existing known host keys file
        if self.autoadd:  # if the user has passed true and is comfortable trusting the host key
            self.ssh.set_missing_host_key_policy(
                paramiko.AutoAddPolicy())  # set this host key to known so that we may connect

        try:
            sys.stdout.write("\rConnecting...Validating Credentials..." + Bcolors.ENDC)
            sys.stdout.flush()
            # connect to the given host with the given credentials via SSH
            self.ssh.connect(self.hostname, username=self.username, password=self.password)
            self.connected = True
            sys.stdout.write("\rConnecting...Validated Credentials..." + Bcolors.GREEN + "Connected\n" + Bcolors.ENDC)
            sys.stdout.flush()
        except paramiko.AuthenticationException:  # could not log into the connection because of invalid credentials
            self.connection_error = "The login credentials provided for {" + self.hostname + "} failed to authenticate."
            self.diagnostic_logger.error(self.connection_error)
            self.connected = False
            sys.stdout.write(Bcolors.RED + "\rConnecting...Validating Credentials...Failed.\n" + Bcolors.ENDC)
            sys.stdout.flush()
        except socket.gaierror:  # could not reach the hostname address provided
            self.connection_error = "The script failed to reach the address: {" + self.hostname + "}"
            self.diagnostic_logger.error(self.connection_error)
            self.connected = False
            sys.stdout.write(Bcolors.RED + "\rConnecting...Validating Credentials...Invalid address.\n" + Bcolors.ENDC)
            sys.stdout.flush()
        except socket.timeout:  # socket timed out while trying to connect / login
            self.connection_error = "The connection attempt to {" + self.hostname + "} timed out"
            self.diagnostic_logger.error(self.connection_error)
            self.connected = False
            sys.stdout.write(Bcolors.RED + "\rConnecting...Validating Credentials...Timed out.\n" + Bcolors.ENDC)
            sys.stdout.flush()
        except paramiko.SSHException:
        # this error is generated when paramiko doesn't receive a protocol banner, or the sever sends something invalid.
        # If the server is otherwise working correctly, this may be due to some network restrictions
            self.connection_error = "Didn't receive/received invalid protocol banner from {" + self.hostname + "}"
            self.diagnostic_logger.error(self.connection_error)
            self.connected = False
            sys.stdout.write(Bcolors.RED + "\rConnecting...Validating Credentials...Invalid protocol banner.\n")
            sys.stdout.flush()
        except Exception as e:
            self.connection_error = str(e) + " {" + self.hostname + "}"
            self.diagnostic_logger.error(self.connection_error)
            self.connected = False
            sys.stdout.write(Bcolors.RED + "\rConnecting...Validating Credentials..." + str(e))
            sys.stdout.flush()

    def run(self):
        print("This is the sshconnection's run method. Each child class must implement their own version.")

    def run_weekly(self):
        print("This is the sshconnection's run_weekly method. Each child class must implement their own version.")

    def display_error(self):
        msg = "Couldn't execute any commands on this connection because: " + self.connection_error
        self.diagnostic_logger.warning(msg)
        self.log(self.datafile, True, msg + "\n" + self.suffix)

    def open_filter_script_code(self):
        try:
            open('mini-scripts/filter_oracle_logs.py')
            exists = True
        except:
            exists = False

        if exists:
            with open('mini-scripts/filter_oracle_logs.py', 'r+') as codefile:
                for line in codefile.readlines():
                    self.filter_code += line
        else:
            print(Bcolors.RED + Bcolors.BLINK_SLOW + "COULD NOT FIND LOCAL FILTER MINI-SCRIPT AT " + Bcolors.BLINK_OFF + "mini-scripts/filter_oracle_logs.py" + Bcolors.ENDC)

    # this function takes the local git copy of the filtering python script,
    # and write it to the home directory of the user executing this script on the remote connection
    # that script will then be used to filter out most of the unnecessary lines from the logs
    def write_filter_script_to_remote(self):
        print(Bcolors.HEADER + "Updating remote filtering script with local copy." + Bcolors.ENDC)
        self.open_filter_script_code()
        cmd = 'echo "' + self.filter_code + '" > /home/' + self.username + '/filter_oracle_logs.py'
        self.execute(cmd)
        if "Permission denied" in self.output:
            print(Bcolors.RED + Bcolors.BLINK_SLOW + "COULD NOT OVERWRITE FILTER MINI-SCRIPT.  PERMISSION DENIED." + Bcolors.ENDC)
        if self.output:
            print(self.output)

    # executes the command provided in the params on the remote connection
    # stores response in self.output
    def execute(self, command):
        self.stdin, self.stdout, self.stderr = self.ssh.exec_command(
            command)  # execute the command given and assign stdin, stdout, and stderr to link up
        self.output = self.stdout.read()  # saves stdout to a variable temporarily
        if self.output == '':
            self.output = self.stderr.read()
        self.diagnostic_logger.debug("executed command = (" + command + ") successfully")

    # executes the command provided in the params on the remote connection as a parameter of the sudo command
    # this requires a pseudo tty to be created and all commands executed through it
    # if user is provided and not None, the sudo command will login as the provided user
    def sudo_execute(self, cmd, user=None):
        if not user is None:
            command = "sudo -u " + user + " " + cmd
        else:
            command = "sudo " + cmd
        self.channel = self.ssh.get_transport().open_session()
        sys.stdout.write(Bcolors.BLINK_SLOW + "\rGetting pseudo tty...     " + Bcolors.ENDC)
        sys.stdout.flush()
        self.channel.get_pty()  # get pseudo tty = pty
        self.channel.exec_command(command)  # this will prompt for the sudoer's password
        self.channel.send("%s\n" % self.password)  # send the sudoer's password
        sys.stdout.write(Bcolors.BLINK_SLOW + "\rPreparing channel...      " + Bcolors.ENDC)
        sys.stdout.flush()
        while not self.channel.recv_ready():  # wait until the channel is ready to receive information
            time.sleep(1)

        if not self.concise:
            sys.stdout.write("\rCommand was [ " + Bcolors.BLUE + command + Bcolors.ENDC + " ]\n")
            sys.stdout.flush()

        self.output = ""
        sys.stdout.write(Bcolors.BLINK_SLOW + 'Preparing pipe...' + Bcolors.ENDC)
        sys.stdout.flush()
        block = self.channel.recv(8192)  # 8192 is arbitrarily selected, not particularly important what size block is
        sumbytes = 8192
        while block != "":  # data must be pulled via recv() unfortunately, so this pulls blocks of 1024 until exhausted
            self.output += block
            block = self.channel.recv(8192)
            sumbytes += 8192
            sys.stdout.write('\r' + Bcolors.BLINK_SLOW + 'Downloading...' + Bcolors.ENDC + ' ' + Bcolors.YELLOW + str(sumbytes) + Bcolors.ENDC + ' bytes')
            sys.stdout.flush()
        sys.stdout.write("\rDownloading... " + Bcolors.GREEN_HIGH + str(sumbytes) + Bcolors.ENDC + " bytes >> Done\n")
        sys.stdout.flush()
        self.output = re.sub('"', "'", self.output)  # change all double quotes to single quotes
        if self.output.splitlines()[0] == "[sudo] password for " + self.username + ": ":
            self.output = '\n'.join(self.output.splitlines()[1:])

    def check_csv(self, service):
        try:
            open(self.infile, 'r')
            exists = True
        except:
            exists = False

        if exists:
            with open(self.infile, 'rb') as csvfile:
                csvreader = csv.reader(csvfile, delimiter=',', quotechar='"')
                for row in csvreader:
                    if ((row[0] == service) or row[0] == 'ALL') and (row[2].strip() != ''):
                        matched = 0
                        compiled = False
                        try:
                            regex = re.compile(row[2])
                            compiled = True
                            if regex.search(row[1]):
                                pass
                            else:
                                print(Bcolors.RED + regex.pattern + Bcolors.ENDC + " does not match "
                                      + Bcolors.RED + row[1] + Bcolors.ENDC)
                            with open(self.infile, 'r') as temp:
                                tempreader = csv.reader(temp, delimiter=',', quotechar='"')
                                for temprow in tempreader:
                                    if ((temprow[0] == service) or temprow[0] == 'ALL') and (temprow[2].strip() != ''):
                                        if regex.search(temprow[1]):
                                            matched += 1
                        except re.error:
                            print("Couldn't compile " + Bcolors.YELLOW + row[2])

                        if matched == 0 and not compiled:
                            print(row[2] + Bcolors.RED + " could not be compiled, so it doesn't match any errors.")
                        elif matched == 0 and compiled:
                            print(row[2] + Bcolors.YELLOW + " doesn't match any errors.")
                        elif matched > 1:
                            print(row[2] + Bcolors.HEADER + " matches " + str(matched)
                                  + " errors.  Please check the regex or remove the duplicate row.")
                        else:
                            pass
                        sys.stdout.write(Bcolors.ENDC)
                        sys.stdout.flush()
        else:
            print(Bcolors.RED + Bcolors.BLINK_SLOW + "NO CSV SUPPLIED FOR ANALYSIS (INFILE)" + Bcolors.ENDC)

    # this functions reads the infile provided by cmd line args, if given
    # it parses each line and creates custom aggregate data structure "OracleError", assigning necessary variables
    # it then adds them to the list of documented errors that the script later checks against
    def read_csv(self, service):
        self.check_csv(service)
        try:
            open(self.infile, 'r')
            exists = True
        except:
            exists = False

        if exists:
            with open(self.infile, 'rb') as csvfile:
                spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
                for row in spamreader:
                    if ((row[0] == service) or row[0] == 'ALL') and (row[2].strip() != ''):
                        if row[3] == '2':
                            self.documented_errors.append(
                                OracleError(row[2], description=row[5], action=row[4], status='alert'))
                        elif row[3] == '1':
                            self.documented_errors.append(
                                OracleError(row[2], description=row[5], action=row[4], status='benign'))
                        elif row[3] == '0':
                            self.blacklist.append(OracleError(row[2], description=row[5], action=row[4]))
                        elif row[3] == '?' or row[3] == '':
                            self.documented_errors.append(
                                OracleError(row[2], description=row[5], action=row[4], status='unknown'))
                        else:
                            print(Bcolors.YELLOW)
                            print(row)
                            # print(row[3])

    # this function writes the provided row in params to the csv, consisting of Service, FullError, and Identifier
    # almost always, the Identifier will need to be checked and edited by a human to make it right,
    # because it is generally too specific
    def write_csv(self, row):
        try:
            open(self.infile, 'r')
            exists = True
        except:
            exists = False

        if exists:
            with open(self.infile, 'a') as csvfile:
                csvfile.write('\n' + row)

    # this function graphs the provided error
    def graph(self):
        pass

    # this function checks the various memory usage metrics of the connection, namely:
    #   top
    #   free
    #   df
    def check_mem(self, superuser=None):
        print("***************************")
        print("* M E M O R Y   C H E C K *")
        print("***************************")
        if not superuser:
            superuser = self.superuser
        # the following bash command is split into 2 lines to be PEP-8 compliant
        get_mem_cmd = "COLUMNS=1000 top -c -u " + superuser + " -n1 -b | tail -n+8 |" \
                      " awk '{if($0 != \"\" && $10 != \"0.0\") print $0;}'"  # get the memory use of each process > 0.0%
        self.stdin, self.stdout, self.stderr = self.ssh.exec_command(get_mem_cmd)
        self.output = self.stdout.read()
        checked = []
        for name in self.memchecks:  # for all the processes we are supposed to check for
            if name[0] not in checked:  # if this process wasn't already checked
                checked.append(name[0])
                found = 0  # number of process with the given name found
                num_processes = 0
                # this for loop counts up any duplicates there may be in the
                # processes to check so we do not falsely report duplicate processes
                for process in self.memchecks:
                    if name[0] == process[0]:
                        num_processes += 1
                for line in self.output.splitlines():  # split by newline
                    columns = line.split()  # split by whitespace
                    # if the name of this process is the name of the process we're currently looking at,
                    # process is columns[11:] because if the command has spaces in it,
                    # it will get split into further columns by the python code.
                    # [11:] means from index 11 until the end of the list
                    if name[0] in columns[11:]:
                        found += 1
                        self.process_top_data(name, columns)

                if found == 0:  # the process we were looking for was never found
                    msg = "[" + name[0] + "] NOT FOUND IN SYSTEM MEMORY CHECK"
                    self.diagnostic_logger.error(msg)
                    self.log(self.datafile, True, msg)
                elif found > num_processes:  # duplicate processes of this name were found
                    msg = "\n        !!! DUPLICATE PROCESSES FOUND FOR [" + name[0] + "]\n"
                    self.diagnostic_logger.error(msg)
                    self.log(self.datafile, True, msg)

        self.check_system_memory()

        self.check_disk_space()

        self.diagnostic_logger.info("checked system memory")
        print("")

    # this function processes the output of the top command into a human-readable report
    def process_top_data(self, name, columns):
        msg = "[" + name[0] + "]:"
        self.diagnostic_logger.info(msg)
        self.log(self.datafile, True, msg)

        msg = "    -> Using " + columns[9] + "% of system memory."
        self.diagnostic_logger.info(msg)
        self.log(self.datafile, True, msg)

        if columns[4][-1] == 'm':
            allotted = float(columns[4][:-1]) / 1000
        elif columns[4][-1] == 'g':
            allotted = float(columns[4][:-1])
        else:
            allotted = columns[4]

        if columns[5][-1] == 'm':
            used = float(columns[5][:-1]) / 1000
        elif columns[5][-1] == 'g':
            used = float(columns[5][:-1])
        else:
            used = columns[5]

        msg = "    -> Using " + str(100 * used / allotted) + "% of allotted memory (" + str(used) + "/" + str(
            allotted) + " GB)"
        print(msg)
        remaining_mem = allotted - used
        if remaining_mem < 1.0:
            msg = Bcolors.RED + "    CRITICAL: " + name[0] + " has < 1 GB free of its allotted memory" + Bcolors.ENDC
            print(msg)
        elif remaining_mem < 3.0:
            msg = Bcolors.YELLOW + "    WARNING: " + name[0] + " has < 3 GB free of its allotted memory" + Bcolors.ENDC
            print(msg)

        msg = "    -> Using " + str(100 * float(columns[8]) / 800.0) + "% of system CPU (" + columns[8] + "/800%)"
        print(msg)

    # this function processes the output of the free command into a human-readable report
    def check_system_memory(self):
        get_mem_cmd = "free -m"
        self.stdin, self.stdout, self.stderr = self.ssh.exec_command(get_mem_cmd)
        self.output = self.stdout.read()
        for line in self.output.splitlines()[1:]:
            columns = line.split()
            # row_name = columns[0]
            row_total = columns[1]
            row_used = columns[2]
            row_free = columns[3]

            if row_total == "buffers/cache:":
                msg = "" + str(int(100 * float(row_free) / (
                    float(row_used) + float(row_free)))) + "% of system memory free after -/+ cache"
                print(msg)

    # this function processes the output of the df command into a human-readable report
    def check_disk_space(self):
        get_disk_space = "df -h"
        sys.stdout.write("Checking disk space...")
        sys.stdout.flush()
        self.stdin, self.stdout, self.stderr = self.ssh.exec_command(get_disk_space)
        self.output = self.stdout.read()
        for line in self.output.splitlines()[1:]:
            columns = line.split()
            length = len(columns)
            if length > 1:  # this is to check length of columns because long processes can sometimes jump to new rows
                if int(columns[length - 2].strip('%')) > 90:
                    msg = "Mount " + columns[length - 1] + " is using " + columns[length - 2] + \
                          " of its disk space (" + columns[length - 4] + "/" + columns[length - 5] + ")"
                    self.diagnostic_logger.error(msg)
                    self.log(self.datafile, True, Bcolors.RED + "CRITICALLY LOW(>90%)\n" + msg + Bcolors.ENDC)
                elif int(columns[length - 2].strip('%')) > 80:
                    msg = "Mount " + columns[length - 1] + " is using " + columns[length - 2] + \
                          " of its disk space (" + columns[length - 4] + "/" + columns[length - 5] + ")"
                    self.diagnostic_logger.warning(msg)
                    self.log(self.datafile, True, Bcolors.YELLOW + "DISK SPACE RUNNING LOW(>80%)\n" + msg + Bcolors.ENDC)

        sys.stdout.write(Bcolors.GREEN + "\rChecking disk space...Done" + Bcolors.ENDC)
        sys.stdout.flush()

    # this function logs the provided message to the provided filename, and if print_to_console is true,
    # it will also print the same message to the console
    def log(self, filename, print_to_console, msg):
        if print_to_console:
            print(msg)
        # timestamp = str(datetime.datetime.now()).split('.')[0]
        line = re.sub(r"\033\[\d*m", "", msg)
        with open(filename, 'a') as logfile:
            logfile.write(line + "\n")
        with open(self.outfile, 'a') as daily:
            daily.write(line + "\n")

    def get_logs(self):
        self.log(self.datafile, True, "This is the generic sshconnection's get_logs method.  Each child class must implement its own.")

    def get_weekly_logs(self):
        self.log(self.datafile, True, "This is the generic sshconnection's get_weekly_logs method.  Each child class must implement its own.")

    def check_weekly_logs(self):
        print(Bcolors.CYAN_BG + self.hostname + Bcolors.ENDC)
        lines = ""
        for log in self.logs:
            opened = False
            try:
                f = open(log, "r")
                print(Bcolors.GREEN + "Read " + log + Bcolors.ENDC)
                opened = True
            except IOError as e:
                print(Bcolors.RED + Bcolors.BLINK_SLOW + "No local copy of " + log + Bcolors.ENDC)
            except:
                print("Unexpected error:", sys.exc_info()[0])

            if opened:
                lines += f.read()

        self.parse_errors(lines)

    # this function puts together the array of ignorable errors and sets up the logs to be parsed
    def check_logs(self, superuser=None):
        if not superuser:
            superuser = self.superuser
        self.write_filter_script_to_remote()  # write the local git copy of the filtering script to the connection
        if not self.blacklist:
            ignore = ""
        else:
            ignore = " \""
            for error in self.blacklist:
                ignore += error.identifier + '\|'
            ignore = ignore[:-2] + "\""  # chop off the last unnecessary '\|'

        if self.command is None:
            self.log(self.datafile, True, Bcolors.ENDC + "The child class's self.command variable has not been set, so nothing can be executed")
        else:
            htmlfile = open(self.htmlfile, "a")
            htmlfile.write("<h2>" + self.hostname + "</h2>")
            for log in self.logs:
                self.log(self.datafile, True, Bcolors.ENDC + "===   " + log + "   ===")
                command = self.command + " " + log + ignore
                self.sudo_execute(command, user=superuser)
                with open("filtered/" + self.hostname + "/" + log.split('/')[-1], 'w') as local_log:
                    local_log.write(self.output)
                self.parse_errors(self.output)

    # parses the log starting at the provided line until the first instance of the characters ']]'
    # thus capturing the entire stacktrace
    # if the stacktrace contains a "caused by:" line, it captures that as well in its own variable
    @staticmethod
    def get_stacktrace(log, line):
        trace = log[line-1].split('] ')[-1] + '\n'
        cause = ''
        while line < len(log) and log[line].strip() != ']]':
            if "Caused by: " in log[line]:
                cause = log[line]
            trace += log[line] + '\n'
            line += 1
        trace += ']]\n'
        return trace, cause

    # this function parses the lines pulled from the given log and displays any necessary output
    def parse_errors(self, errors):
        self.read_csv(self.service)
        number_of_errors = 0
        alert_errors = []
        benign_errors = []
        unknown_errors = []
        new_errors = []
        i = 0
        lines = errors.splitlines()[1:]

        for row in self.documented_errors:
            row.wipe()
            if row.status != 'new':
                try:
                    row.compiled_pattern = re.compile(row.identifier)
                    row.is_regex_compiled = True
                except re.error:
                    print("COULD NOT COMPILE -    " + Bcolors.RED + row.identifier + Bcolors.ENDC)
                    row.is_regex_compiled = False
            else:
                row.is_regex_compiled = False

        for line in lines:
            i += 1
            progress = int(100 * float(i)/len(lines))
            if progress == 100:
                sys.stdout.write('\rProcessed ' + Bcolors.GREEN + str(i) + Bcolors.ENDC + '/' + Bcolors.GREEN
                                 + str(len(lines)) + Bcolors.ENDC + ' lines -> ' + str(progress) + '%')
            else:
                sys.stdout.write('\rProcessed ' + Bcolors.YELLOW + str(i) + Bcolors.ENDC + '/' + Bcolors.ENDC
                                 + str(len(lines)) + ' lines -> ' + str(progress) + '%')
            sys.stdout.flush()

            if ("[sudo] password for " not in line) and (self.password not in line):
                if ("[ERROR]" in line) or ("[SEVERE]" in line):
                    matched = False
                    stack_available = False
                    error = line.split('] ')[-1]

                    if error.strip() != '':
                        if '[[' in error:
                            stack_available = True

                        trace = ''
                        cause = ''

                        for entry in self.documented_errors:
                            if entry.is_regex_compiled:
                                if entry.compiled_pattern.search(line):
                                    if stack_available:
                                        trace, cause = self.get_stacktrace(lines, i)
                                        if (cause != '') and (entry.caused_by != '') and (cause != entry.caused_by):  # if this entry's cause is different from the cause of the log message currently being inspected
                                            pass
                                        else:  # this entry's cause has not been set, so it is fine to set
                                            entry.stacktrace, entry.caused_by = trace, cause
                                            matched = True

                                    if matched or not stack_available:
                                        number_of_errors += 1
                                        entry.occurrences += 1
                                        entry.timestamps += line.split('] ')[0] + ']\n'
                                        entry.timestamp_header = '\n\n' + line.split('] ')[0].split('T')[0] + '  GMT-'\
                                                                 + line.split('] ')[0].split('T')[1].split('-')[1] + ']\n'
                                        entry.full_message = line
                                        entry.line_number = i

                                        matched = True
                                        break
                                else:  # current log line did not match this entry, move on
                                    pass
                            else:
                                if entry.identifier in line:
                                    if stack_available:
                                        trace, cause = self.get_stacktrace(lines, i)
                                        if (cause != '') and (entry.caused_by != '') and (cause != entry.caused_by):  # if this entry's cause is different from the cause of the log message currently being inspected
                                            pass
                                        else:  # this entry's cause has not been set, so it is fine to set
                                            entry.stacktrace, entry.caused_by = trace, cause
                                            matched = True

                                    if matched or not stack_available:
                                        number_of_errors += 1
                                        entry.occurrences += 1
                                        entry.timestamps += line.split('] ')[0] + ']\n'
                                        entry.timestamp_header = '\n\n' + line.split('] ')[0].split('T')[0] + '  GMT-'\
                                                                 + line.split('] ')[0].split('T')[1].split('-')[1] + ']\n'
                                        entry.full_message = line
                                        entry.line_number = i

                                        matched = True
                                        break
                                else:  # current log line did not match this entry, move on
                                    pass
                        if not matched:
                            number_of_errors += 1
                            if stack_available:
                                trace, cause = self.get_stacktrace(lines, i)
                            if '[[' in error:
                                error = error.split('[[')[0]
                            self.documented_errors.append(
                                OracleError(error, occurrences=1, timestamp_header='',
                                            timestamps=line.split('] ')[0] + ']\n', stacktrace=trace, caused_by=cause,
                                            is_regex_compiled=False, full_message=line, line_number=i))

        if errors == "FILE DOES NOT EXIST":
            self.log(self.datafile, True, "\n        " + Bcolors.RED + "FILE DOES NOT EXIST\n\n" + Bcolors.ENDC)
        elif number_of_errors == 0:
            self.log(self.datafile, True, "\n        No errors or severe errors found for this log.\n")
        else:
            for row in self.documented_errors:
                if row.occurrences > 0:
                    if row.status == 'alert':
                        alert_errors.append(row)
                    elif row.status == 'benign':
                        benign_errors.append(row)
                    elif row.status == 'unknown':
                        unknown_errors.append(row)
                    elif row.status == 'new':
                        new_errors.append(row)
                    else:
                        print("Invalid error status")

            htmlfile = open(self.htmlfile, "a")

            print('')
            if len(alert_errors) > 0:
                self.log(self.datafile, True, Bcolors.RED_BG_HIGH +
                         "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n" +
                         "                                ALERT                                      \n" +
                         "vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv\n" + Bcolors.ENDC)
                htmlfile.write("<p style='color:white; background:red; text-align:center; font:14pt;'>ALERT</p><pre>")
                for row in alert_errors:
                    print(Bcolors.ENDC + Bcolors.RED)
                    htmlfile.write("<span style='color:red'>")
                    self.log(self.datafile, True, "Error: " + row.identifier)
                    htmlfile.write("<strong style='color:black'>Error:</strong> " + row.identifier + "<br>")
                    self.log(self.datafile, True, "Description: " + Bcolors.ENDC + row.description + Bcolors.RED)
                    htmlfile.write("<strong style='color:black'>Description:</strong></span> " + row.description + "<br>")
                    self.log(self.datafile, True, "Occurrences: " + str(row.occurrences))
                    htmlfile.write("<span style='color:red'><strong style='color:black'>Occurrences:</strong> " + str(row.occurrences) + "<br>")
                    self.log(self.datafile, True, "Between: " + row.timestamps.split('\n')[0]
                             + " and " + row.timestamps.split('\n')[-2])
                    htmlfile.write("<strong style='color:black'>Between:</strong> " + row.timestamps.split('\n')[0] + " and " + row.timestamps.split('\n')[-2] + "<br>")
                    if row.action:
                        self.log(self.datafile, True, "Recommended Action: " + Bcolors.ENDC + row.action + Bcolors.RED)
                        htmlfile.write("<strong style='color:black'>Recommended Action:</strong> " + row.action + "<br>")
                    if self.display_timestamps:
                        self.log(self.datafile, True, row.timestamp_header + '\n' + row.timestamps)
                        htmlfile.write(row.timestamp_header + '<br>' + '<br>'.join(row.timestamps.split('\n')) + "<br>")
                    if (self.display_stacktrace or (row.full_message.split('] ')[-1].strip() == '[[')) and row.stacktrace != '':
                        if row.caused_by != '':
                            self.log(self.datafile, True, "Caused by: " + Bcolors.ENDC
                                     + row.caused_by.split("Caused by: ")[1] + Bcolors.RED)
                            # htmlfile.write("<strong style='color:black'>Caused by:</strong> " + row.caused_by.split("Caused by: ")[1] + "<br>")
                        self.log(self.datafile, True, "Stacktrace:\n" + Bcolors.ENDC + row.stacktrace + Bcolors.RED)
                        # htmlfile.write("<strong style='color:black'>Stacktrace:</strong><br>" + '<br>'.join(row.stacktrace.split('\n')) + "<br>")
                    self.log(self.datafile, True, "----------------------------------------" + Bcolors.ENDC)
                    htmlfile.write("<hr><br>")
                    htmlfile.write("</span>")

            if len(benign_errors) > 0:
                self.log(self.datafile, True, Bcolors.BLUE_BG_HIGH +
                         "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n" +
                         "                               BENIGN                                      \n" +
                         "vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv\n" + Bcolors.ENDC)
                htmlfile.write("</pre><p style='color:white; background:blue; text-align:center; font:14pt;'>BENIGN</p><pre>")
                for row in benign_errors:
                    print(Bcolors.ENDC + Bcolors.BLUE_HIGH)
                    htmlfile.write("<span style='color:blue'>")
                    self.log(self.datafile, True, "Error: " + row.identifier)
                    htmlfile.write("<strong style='color:black'>Error:</strong> " + row.identifier + "<br>")
                    self.log(self.datafile, True, "Description: " + Bcolors.ENDC + row.description + Bcolors.BLUE_HIGH)
                    htmlfile.write("<strong style='color:black'>Description:</strong></span> " + row.description + "<br>")
                    self.log(self.datafile, True, "Occurrences: " + str(row.occurrences))
                    htmlfile.write("<span style='color:blue'><strong style='color:black'>Occurrences:</strong> " + str(row.occurrences) + "<br>")
                    self.log(self.datafile, True, "Between: " + row.timestamps.split('\n')[0]
                             + " and " + row.timestamps.split('\n')[-2])
                    htmlfile.write("<strong style='color:black'>Between:</strong> " + row.timestamps.split('\n')[0] + " and " + row.timestamps.split('\n')[-2] + "<br>")
                    if row.action:
                        self.log(self.datafile, True, "Recommended Action: "
                                 + Bcolors.ENDC + row.action + Bcolors.BLUE_HIGH)
                        htmlfile.write("<strong style='color:black'>Recommended Action:</strong> " + row.action + "<br>")
                    if self.display_timestamps:
                        self.log(self.datafile, True, row.timestamp_header + '\n' + row.timestamps)
                        htmlfile.write(row.timestamp_header + '<br>' + '<br>'.join(row.timestamps.split('\n')) + "<br>")
                    if (self.display_stacktrace or ((not self.display_stacktrace) and (row.full_message.split('] ')[-1].strip() == '[['))) and row.stacktrace != '':
                        if row.caused_by != '':
                            self.log(self.datafile, True, "Caused by: " + Bcolors.ENDC
                                     + row.caused_by.split("Caused by: ")[1] + Bcolors.BLUE_HIGH)
                            # htmlfile.write("<strong style='color:black'>Caused by:</strong> " + row.caused_by.split("Caused by: ")[1] + "<br>")
                        self.log(self.datafile, True, "Stacktrace:\n" + Bcolors.ENDC + row.stacktrace + Bcolors.BLUE_HIGH)
                        # htmlfile.write("<strong style='color:black'>Stacktrace:</strong><br>" + '<br>'.join(row.stacktrace.split('\n')) + "<br>")
                    self.log(self.datafile, True, "----------------------------------------" + Bcolors.ENDC)
                    htmlfile.write("<hr><br>")
                    htmlfile.write("</span>")

            if len(unknown_errors) > 0:
                self.log(self.datafile, True, Bcolors.YELLOW_BG_HIGH +
                         "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n" +
                         "                               UNKNOWN                                     \n" +
                         "vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv\n" + Bcolors.ENDC)
                htmlfile.write("</pre><p style='color:yellow; background:black; text-align:center; font:14pt;'>NOT ENOUGH INFORMATION</p><pre>")
                for row in unknown_errors:
                    print(Bcolors.ENDC + Bcolors.YELLOW)
                    htmlfile.write("<span style='color:gray'>")
                    self.log(self.datafile, True, "Error: " + row.identifier)
                    htmlfile.write("<strong style='color:black'>Error:</strong> " + row.identifier + "<br>")
                    self.log(self.datafile, True, "Description: " + Bcolors.ENDC + row.description + Bcolors.YELLOW)
                    htmlfile.write("<strong style='color:black'>Description:</strong></span> " + row.description + "<br>")
                    self.log(self.datafile, True, "Occurrences: " + str(row.occurrences))
                    htmlfile.write("<span style='color:gray'><strong style='color:black'>Occurrences:</strong> " + str(row.occurrences) + "<br>")
                    self.log(self.datafile, True, "Between: " + row.timestamps.split('\n')[0]
                             + " and " + row.timestamps.split('\n')[-2])
                    htmlfile.write("<strong style='color:black'>Between:</strong> " + row.timestamps.split('\n')[0] + " and " + row.timestamps.split('\n')[-2] + "<br>")
                    if row.action:
                        self.log(self.datafile, True, "Recommended Action: "
                                 + Bcolors.ENDC + row.action + Bcolors.YELLOW)
                        htmlfile.write("<strong style='color:black'>Recommended Action:</strong> " + row.action + "<br>")
                    if self.display_timestamps:
                        self.log(self.datafile, True, row.timestamp_header + '\n' + row.timestamps)
                        htmlfile.write(row.timestamp_header + '<br>' + '<br>'.join(row.timestamps.split('\n')) + "<br>")
                    if (self.display_stacktrace or ((not self.display_stacktrace) and (row.full_message.split('] ')[-1].strip() == '[['))) and row.stacktrace != '':
                        if row.caused_by != '':
                            self.log(self.datafile, True, "Caused by: " + Bcolors.ENDC
                                     + row.caused_by.split("Caused by: ")[1] + Bcolors.YELLOW)
                            # htmlfile.write("<strong style='color:black'>Caused by:</strong> " + row.caused_by.split("Caused by: ")[1] + "<br>")
                        self.log(self.datafile, True, "Stacktrace:\n" + Bcolors.ENDC + row.stacktrace + Bcolors.YELLOW)
                        # htmlfile.write("<strong style='color:black'>Stacktrace:</strong><br>" + '<br>'.join(row.stacktrace.split('\n')) + "<br>")
                    self.log(self.datafile, True, "----------------------------------------" + Bcolors.ENDC)
                    htmlfile.write("<hr><br>")
                    htmlfile.write("</span>")

            if len(new_errors) > 0:
                self.log(self.datafile, True, Bcolors.HEADER_BG_HIGH +
                         "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n" +
                         "                                 NEW                                       \n" +
                         "vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv\n")
                htmlfile.write("</pre><p style='color:white; background:pink; text-align:center; font:14pt;'>NEW</p><pre>")
                for row in new_errors:
                    print(Bcolors.ENDC + Bcolors.HEADER)
                    htmlfile.write("<span style='color:purple'>")
                    self.log(self.datafile, True, "Line: " + str(row.line_number))
                    htmlfile.write("<strong style='color:black'>Line:</strong> " + str(row.line_number) + "<br>")
                    self.log(self.datafile, True, row.full_message)
                    htmlfile.write("<strong style='color:black'>Full Message:</strong> " + row.full_message + "<br>")
                    self.log(self.datafile, True, "Occurrences: " + str(row.occurrences))
                    htmlfile.write("<strong style='color:black'>Occurrences:</strong> " + str(row.occurrences) + "<br>")
                    self.log(self.datafile, True, "Between: " + row.timestamps.split('\n')[0]
                             + " and " + row.timestamps.split('\n')[-2])
                    htmlfile.write("<strong style='color:black'>Between:</strong> " + row.timestamps.split('\n')[0] + " and " + row.timestamps.split('\n')[-2] + "<br>")
                    if self.display_timestamps:
                        self.log(self.datafile, True, row.timestamp_header + '\n' + row.timestamps)
                        htmlfile.write(row.timestamp_header + '<br>' + '<br>'.join(row.timestamps.split('\n')) + "<br>")
                    if (self.display_stacktrace or ((not self.display_stacktrace) and (row.full_message.split('] ')[-1].strip() == '[['))) and row.stacktrace != '':
                        if row.caused_by != '':
                            self.log(self.datafile, True, "Caused by: " + Bcolors.ENDC
                                     + row.caused_by.split("Caused by: ")[1] + Bcolors.HEADER)
                            # htmlfile.write("<strong style='color:black'>Caused by:</strong> " + row.caused_by.split("Caused by: ")[1] + "<br>")
                        self.log(self.datafile, True, "Stacktrace:\n" + Bcolors.ENDC + row.stacktrace + Bcolors.HEADER)
                        # htmlfile.write("<strong style='color:black'>Stacktrace:</strong><br>" + '<br>'.join(row.stacktrace.split('\n')) + "<br>")
                    self.log(self.datafile, True, "----------------------------------------" + Bcolors.ENDC)
                    htmlfile.write("<hr><br>")
                    htmlfile.write("</span>")
                    logmsg = row.full_message.split('] ')[-1].strip()
                    if logmsg != '[[':
                        if "[[" in logmsg:
                            logmsg = logmsg.split('[[')[0].strip()
                        self.write_csv(self.service + ",\"" + row.full_message + "\",\"" + logmsg + "\",?,,")
            print(Bcolors.ENDC + '\n\n')
            htmlfile.write("</pre>")

    def shutdown(self):
        sys.stdout.write("Disconnecting...")
        self.ssh.close()
        sys.stdout.write("\rDisconnecting...Disconnected\n")