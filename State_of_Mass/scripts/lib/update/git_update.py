# Project Name: HCM Managed Support Services
# File Name: git_update.py
# This file provides the functionality for checking if the local repo is behind the remote repo, and updates the code if the user wishes to

from ..console.colors import *
import subprocess
import sys


def git_update():
    pulling = True
    while pulling:
        sys.stdout.write(Bcolors.YELLOW + "Pulling from origin..." + Bcolors.ENDC)
        sys.stdout.flush()
        proc = subprocess.Popen(["git pull origin"], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        output, error = proc.communicate()

        if output.strip() == "Already up-to-date.":
            print(Bcolors.GREEN + output + Bcolors.ENDC)
            pulling = False
        elif "error: Your local changes to the following files would be overwritten by merge" in error:
            print(Bcolors.RED + error + Bcolors.ENDC)
            shouldIStash = True
            while shouldIStash:
                res = raw_input("Do you wish to remove your local changes and try again? " + Bcolors.BLINK_SLOW + "(y/n)" + Bcolors.ENDC)
                if res == 'y':
                    sys.stdout.write(Bcolors.ENDC + "\rDo you wish to remove your local changes and try again? (y/n)\n")
                    sys.stdout.write(Bcolors.YELLOW + "Removing local changes..." + Bcolors.ENDC)
                    sys.stdout.flush()
                    subprocess.call(["git", "reset", "--hard", "origin/master"])
                    shouldIStash = False
                elif res == 'n':
                    sys.stdout.write(Bcolors.ENDC + "\rDo you wish to remove your local changes and try again? (y/n)\n")
                    sys.stdout.flush()
                    shouldIRun = True
                    while shouldIRun:
                        response = raw_input(Bcolors.BLINK_SLOW + "Do you wish to run the script out-of-date? (y/n)" + Bcolors.ENDC)
                        if response == 'y':
                            sys.stdout.write(Bcolors.ENDC + "\rDo you wish to run the script out-of-date? (y/n)\n")
                            sys.stdout.flush()
                            shouldIRun = False
                            shouldIStash = False
                            pulling = False
                        elif response == 'n':
                            sys.stdout.write(Bcolors.ENDC + "\rDo you wish to run the script out-of-date? (y/n)\n")
                            sys.stdout.flush()
                            sys.exit(1)
                        else:
                            pass
                else:
                    pass
        else:
            print(output)
            pulling = False