# Project Name: State of Mass AIMS Support
# File Name: parse.py
# This file provides the functionality for parsing command line arguments and processing their results

import argparse
import time


def parse_args():
    parser = argparse.ArgumentParser(description='Connect to remote hosts via SSH to execute or sudo-execute commands.')
    parser.add_argument('-u', '--username',
                        required=True,
                        nargs=1,
                        help='Username used to log onto remote server')
    parser.add_argument('-p', '--password',
                        nargs=1,
                        help='Password used to log onto remote server')
    parser.add_argument('-s', '--stacktrace',
                        default=True,
                        action='store_false',
                        help='Add this option to opt-out of printing stacktraces for any applicable errors encountered.')
    parser.add_argument('-i', '--infile',
                        default=None,
                        help='File used for comparing errors against')
    parser.add_argument('-o', '--outfile',
                        default='logs/daily_log.txt',
                        help='Filename for script to save all console output to. '
                             'Defaults to daily_log.txt')
    parser.add_argument('-r', '--reporting',
                        default='reporting/' + time.strftime("%Y-%m-%d_%H-%M-%S") + '.html',
                        help='Filename for script to save error data to in html format. Defaults to <TIMESTAMP>.html')
    parser.add_argument('-d', '--days',
                        default=2,
                        help='Specify a number of days back to run log checks on')
    parser.add_argument('-t', '--timestamp',
                        default=False,
                        action='store_true',
                        help='Add this option to print out the timestamps for all errors')
    parser.add_argument('-a', '--autotrust',
                        default=False,
                        action='store_true',
                        help='Automatically trust any new host connections that are not already in your trusted_hosts')
    parser.add_argument('-c', '--concise',
                        default=False,
                        action='store_true',
                        help='Only print to console what is absolutely necessary to see.')

    args = parser.parse_args()
    return args


def check_options(options):
    print('Storing results in ' + options.outfile)
    print('Storing html error data in ' + options.reporting)

    if not options.infile:
        options.infile = ''

    if options.username:
        options.username = options.username[0]

    if options.password:
        options.password = options.password[0]

    return [options.username,
            options.password,
            options.infile,
            options.outfile,
            options.reporting,
            options.days,
            options.stacktrace,
            options.timestamp,
            options.autotrust,
            options.concise]