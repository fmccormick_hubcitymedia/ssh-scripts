# Project Name: State of Mass AIMS Support
# File Name: read_properties.py
# This file provides the functionality for parsing the properties files for State of Mass

from ..console.colors import *


def read_properties(infile):
    i = 0
    connections = []
    processes = []
    end_block = False
    service = ''
    hostname = ''
    log_directory = ''
    log_prefix = ''
    superuser = 'root'

    try:
        open(infile, 'r')
        exists = True
    except:
        exists = False

    if exists:
        with open(infile, 'r') as properties:
            lines = properties.readlines()
            numlines = len(lines)
            for line in lines:
                i += 1
                if line[0] == '#':  # it's a comment
                    pass
                elif line.strip() == '':
                    end_block = True
                else:
                    values = line.split(':')
                    if values[0] == 'service':
                        service = values[1].strip('\n')
                    elif values[0] == 'hostname':
                        hostname = values[1].strip('\n')
                    elif values[0] == 'log_directory':
                        log_directory = values[1].strip('\n')
                    elif values[0] == 'log_prefix':
                        log_prefix = values[1].strip('\n')
                    elif values[0] == 'superuser':
                        superuser = values[1].strip('\n')
                    elif values[0] == 'process':
                        if ',' in values[1]:
                            process = values[1].strip('\n').split(',')
                            processes.append([process[0], process[1]])
                        else:
                            print("Process does not have associated memory use threshold")
                    else:
                        print("Properties file has invalid attribute on line " + str(i))

                if (end_block or (i == numlines)) and (service != '' and hostname != '' and log_directory != '' and log_prefix != ''):
                    connections.append([service, hostname, log_directory, log_prefix, processes, superuser])
                    end_block = False
                    service = ''
                    hostname = ''
                    log_directory = ''
                    log_prefix = ''
                    superuser = 'root'
                    processes = []

                if end_block:
                    end_block = False
    else:
        print(Bcolors.RED + Bcolors.BLINK_SLOW + "PROPERTIES FILE NOT FOUND AT " + Bcolors.BLINK_OFF + infile + Bcolors.ENDC)

    return connections