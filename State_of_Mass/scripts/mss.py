#! /usr/bin/python

# Project Name: State of Mass AIMS Support
# File Name: mss.py
# This script connects to all the servers on the State of Mass project, and executes each server's individual run method
# These methods are determined by the individual service classes, which are instantiated based on the information in properties/mss.properties
#
# The command line arguments -u and -p are required in order to connect to remote machines
# To learn more about the other command line arguments, run "python mss.py -h" or "python mss.py --help"
#
# There is presently no support for private key authentication, although it is hopefully coming soon
#
# There are 2 required python modules in order to run this script, those are:
#       argparse
#       paramiko
# These can easily be installed respectively:
#       sudo pip install argparse
#       sudo pip install paramiko
#
# If pip itself is not installed, it can also easily be installed via:
#       sudo easy_install pip

__author__ = 'Frank McCormick'

from lib.ssh import *
from lib.connections.oam import *
from lib.connections.oim import *
from lib.connections.soa import *
from lib.connections.ohs import *
from lib.connections.ovd import *
from lib.connections.bip import *
from lib.arguments.parse import *
from lib.arguments.read_properties import *
from lib.update.git_update import *
from lib.send_mail import *


def connect_to_vpn():
    subprocess.call(["open", "ConnectToStateOfMassVPN.app/"])
    time.sleep(20)


def disconnect_from_vpn():
    subprocess.call(["open", "DisconnectFromStateOfMassVPN.app/"])
    time.sleep(5)

if __name__ == '__main__':

    connect_to_vpn()

    subj = "AIMS 4.2 Health Check - " + time.strftime("%b %d, %Y")
    bod = "Hi Naveen,<br><br>" \
          "We've run the tests for today. Here are the results:<br><br>" \
          "<strong>Log Analysis</strong><br><br>" \
          "<strong>Console Checks</strong><br><br>" \
          "I was able to log into all web consoles within a few seconds.<br><br>" \
          "<strong>Data Source Checks</strong><br><br>" \
          "All datasources are 'running' at time of check.<br><br>" \
          "<strong>OEM</strong><br><br>" \
          "All targets are running as usual.<br><br><br>" \
          "Thanks,<br>" \
          "Frank"
    to = ["naveenkumar.chandrasekaran@state.ma.us", "EHS-DL-AIMSMonitoring@massmail.state.ma.us",
          "EHS-DL-Middleware@massmail.state.ma.us", "hcm-stateofmass-projectteam@hubcitymedia.com",
          "nicholas.hieter@state.ma.us", "venkatachalam.jegadeesan@state.ma.us", "mss.all@hubcitymedia.com",
          "uma.velu@state.ma.us", "anil.yeladandi@state.ma.us"]


    to_test = ["fmccormick@hubcitymedia.com", "ndayal@hubcitymedia.com"]

    # email = Email(password="Shadowmaster24", from_address="fmccormick@hubcitymedia.com", to_address=to_test, subject=subj, body=bod, attachment="report.html")
    # email.attach("daily_log.txt")
    # email.send_mail()

    test_console_colors()

    git_update()

    args = parse_args()
    flags = check_options(args)

    print("Checking " + str(flags[5]) + " days.")

    connections = []

    open(flags[3], 'w').close()  # blank out the daily log file
    with open(flags[4], 'w') as htmlfile:
        htmlfile.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">\n<html>\n<head>\n\t<meta content='text/html; charset=ISO-8859-1' http-equiv='content-type'>\n\t<title>Health Check AIMS 4.2 - " + time.strftime("%B %d, %Y") + "</title>\n</head>\n<body>\n<h1>Log Analysis</h1>\n")

    servers = read_properties('properties/mss.properties')
    for server in servers:
        logfile = 'logs/' + server[1] + '-logfile.txt'
        diagfile = 'logs/' + server[1] + '-diagfile.txt'
        if server[0] == 'BIP':
            connections.append(BIPConnection(flags, server[1], logfile, diagfile, server[2], server[3], server[4], server[5]))
        elif server[0] == 'OHS':
            connections.append(OHSConnection(flags, server[1], logfile, diagfile, server[2], server[3], server[4], server[5]))
        elif server[0] == 'OVD':
            connections.append(OVDConnection(flags, server[1], logfile, diagfile, server[2], server[3], server[4], server[5]))
        elif server[0] == 'OAM':
            connections.append(OAMConnection(flags, server[1], logfile, diagfile, server[2], server[3], server[4], server[5]))
        elif server[0] == 'SOA':
            connections.append(SOAConnection(flags, server[1], logfile, diagfile, server[2], server[3], server[4], server[5]))
        elif server[0] == 'OIM':
            connections.append(OIMConnection(flags, server[1], logfile, diagfile, server[2], server[3], server[4], server[5]))
        else:
            print("Properties file contains a block with no designated service attribute")

    for connection in connections:
        connection.connect()
        if connection.connected:
            connection.run()
            connection.shutdown()
        else:
            connection.display_error()

    disconnect_from_vpn()

    print(Bcolors.GREEN + "\n                                   #################################")
    print("                                   # S C R I P T   C O M P L E T E #")
    print("                                   #################################\n" + Bcolors.ENDC)