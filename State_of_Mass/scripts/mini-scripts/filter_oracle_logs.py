#! /usr/bin/python

import sys
import re

ignorable = '\|'
filePath = sys.argv[1]
if len(sys.argv) > 2:
    ignorable = sys.argv[2]
    filtering = True
else:
    filtering = False
i = 0
exists = False

try:
    f = open(filePath, 'r')
    exists = True
except:
    print('FILE DOES NOT EXIST')

if exists:
    with open(filePath, 'r') as log:
        lines = log.readlines()
        numlines = len(lines)
        for line in lines:
            line = line.replace('\"', '\'')
            ignore = False
            i += 1
            if filtering:
                for identifier in ignorable.split('\|'):
                    try:
                        regex = re.compile(identifier)
                        if regex.search(line):
                            ignore = True
                    except re.error:
                        if id in line:
                            ignore = True
            if (('[ERROR]' in line) or ('[SEVERE]' in line)) and not ignore:
                msg = line.split('] ')[-1]
                if msg.strip() != '':
                    print(line.rstrip('\n'))
                    if '[[' in msg:
                        j = i
                        while lines[j].strip() != ']]':
                            print(lines[j].rstrip('\n'))
                            j += 1
                        print(']]')