#! /usr/bin/python

# Project Name: State of Mass AIMS Support
# File Name: mss_weekly.py
# This script checks the local machine's /filtered directory for logs already parsed by mss.py for each server on the State of Mass project, and executes each server's individual run_weekly method
# These methods are determined by the individual service classes, which are instantiated based on the information in properties/mss.properties
# However, this is mostly borrow from the architecture of mss.py, and only the service and log prefix variables are really necessary for this weekly run
#
# There is 1 required python module in order to run this script:
#       argparse
# This can easily be installed respectively:
#       sudo pip install argparse
#
# If pip itself is not installed, it can also easily be installed via:
#       sudo easy_install pip

__author__ = 'Frank McCormick'

from lib.ssh import *
from lib.connections.oam import *
from lib.connections.oim import *
from lib.connections.soa import *
from lib.connections.ohs import *
from lib.connections.ovd import *
from lib.connections.bip import *
from lib.arguments.parse import *
from lib.arguments.read_properties import *
from lib.update.git_update import *
from lib.send_mail import *

if __name__ == '__main__':
    test_console_colors()

    git_update()

    connections = []
    args = parse_args()
    flags = check_options(args)

    open(flags[3], 'w').close()  # blank out the daily log file
    with open(flags[4], 'w') as htmlfile:
        htmlfile.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">\n<html>\n<head>\n\t<meta content='text/html; charset=ISO-8859-1' http-equiv='content-type'>\n\t<title>Health Check AIMS 4.2 - " + time.strftime("%B %d, %Y") + "</title>\n</head>\n<body>\n<h1>Log Analysis</h1>\n")

    servers = read_properties('mss.properties')
    for server in servers:
        logfile = 'logs/' + server[1] + '-logfile.txt'
        diagfile = 'logs/' + server[1] + '-diagfile.txt'
        if server[0] == 'BIP':
            connections.append(BIPConnection(flags, server[1], logfile, diagfile, server[2], server[3], server[4]))
        elif server[0] == 'OHS':
            connections.append(OHSConnection(flags, server[1], logfile, diagfile, server[2], server[3], server[4]))
        elif server[0] == 'OVD':
            connections.append(OVDConnection(flags, server[1], logfile, diagfile, server[2], server[3], server[4]))
        elif server[0] == 'OAM':
            connections.append(OAMConnection(flags, server[1], logfile, diagfile, server[2], server[3], server[4]))
        elif server[0] == 'SOA':
            connections.append(SOAConnection(flags, server[1], logfile, diagfile, server[2], server[3], server[4]))
        elif server[0] == 'OIM':
            connections.append(OIMConnection(flags, server[1], logfile, diagfile, server[2], server[3], server[4]))

    for connection in connections:
        connection.run_weekly()

    print(Bcolors.GREEN + "\n                                   #################################")
    print("                                   # S C R I P T   C O M P L E T E #")
    print("                                   #################################\n" + Bcolors.ENDC)