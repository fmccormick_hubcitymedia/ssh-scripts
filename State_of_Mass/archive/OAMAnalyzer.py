#!/usr/bin/python
# Author - Carl Keven Chang
# August 6, 2014
# Hub City Media

import sys
import subprocess
import shlex

filePath = sys.argv[1]

class logAnalyzer:

        #Analyze OAM diagnostic log
        def OAMAnalyze():
		
		def getFullError(error):
			# For showing full error
			grepFull = "grep -a --text -A 4 \"" + error + "\" " + filePath + " | grep -v 'OAM-04036\|OAMSSA-20023\|OAMSSA-20027\|xelsysadm\|JPS-03156\|JPS-03180\|obrareq.cgi\|locked\|ArmeRUNTIME\|oracle.oam.binding'"
			errorMsg = subprocess.Popen(grepFull,shell=True,stdout=subprocess.PIPE,stderr=subprocess.STDOUT)
			outputFull = errorMsg.communicate()[0]
			numberlines = 0
			for line in outputFull.splitlines():
				print line
				numberlines = numberlines + 1
				if numberlines == 4:
					break

                #Grep for errors
                grepErrors = "grep -a --text 'ERROR\|SEVERE' " + filePath + " | grep -v 'OAM-04036\|OAMSSA-20023\|OAMSSA-20027\|xelsysadm\|JPS-03156\|JPS-03180\|obrareq.cgi\|locked\|ArmeRUNTIME\|oracle.oam.binding' | grep 'auth_cred_submit'"
                errors = subprocess.Popen(grepErrors,shell=True,stdout=subprocess.PIPE,stderr=subprocess.STDOUT)
                output1 = errors.communicate()[0]
                dontAdd = False
                errorList = []
                numberOfErrors = 1

                #Parsing error
                for line in output1.splitlines():
			error = ""

                        for char in range(len(line)):
                                if (line[char] == '['):
                                        dontAdd = True
                                elif line[char] == ']' and char < (len(line)-1):
					if (line[char+1] == ' '):
                                        	dontAdd = False
                                if (dontAdd == False):
                                        if (line[char] is not '['):
                                                if (line[char] is not ']'):
                                                        if (line[char-1] is not ']'):
                                                                error = error + line[char]

                        #if error is not in the list of errors
                        if (error not in errorList) and (error != ""):
                                errorList.append(error)
                                print "========================================================================================="
                                print "Error: " + str(numberOfErrors)
                                numberOfErrors = numberOfErrors + 1
                                print ""
                                print "Error Description: " + str(error)
                                print ""
                                print "Full Error: "
				getFullError(error)

                                # For analyzing error's occurences and times of occurences
                                grepNumber = "grep -a --text \"" + error + "\" " + filePath + " | wc -l"
                                grepTime = "grep -a --text \"" + error + "\" " + filePath + " | awk '{print $1}'"
                                number = subprocess.Popen(grepNumber,shell=True,stdout=subprocess.PIPE,stderr=subprocess.STDOUT)
                                time = subprocess.Popen(grepTime,shell=True,stdout=subprocess.PIPE,stderr=subprocess.STDOUT)
                                output2 = number.communicate()[0]
                                output3 = time.communicate()[0]
                                print ""
                                print "Occurrences: " + output2
                                print "Time: "
                                print  output3
                                print "========================================================================================="

                return

        OAMAnalyze();