#!/usr/bin/python
# Author - Carl Keven Chang
# August 15, 2014
# Hub City Media

import sys
import subprocess
import shlex

filePath = sys.argv[1] # pull the file to be parsed from the first parameter of the arguments

class logAnalyzer:

        #Analyze OIM diagnostic log
        def LogAnalyze():

                #Grep for errors
                grepErrors = "grep -a --text 'ERROR\|SEVERE' " + filePath # return only the lines of the log that contain the keyword ERROR or SEVERE
                child = subprocess.Popen(grepErrors,shell=True,stdout=subprocess.PIPE,stderr=subprocess.STDOUT) # execute the above bash command in a new child process
                lines = child.communicate()[0] # communicate returns a tuple of [stdout, stderr], for our purposes, we only care about the stdout results

                errorList = [] # set this up as an array
                numberOfErrors = 0 # initialize this to 0, it is used later to denote what number error we are looking at
                outputMsgs = [] # set this up as an array

                #Parsing errors
                for line in lines.splitlines():
                        dontAdd = False
                        error = ""
                        if ('ERROR' in line) or ('SEVERE' in line):
                                for char in range(len(line)):
                                        if (line[char] == '['):
                                                dontAdd = True
                                        elif (line[char] == ']') and (line[char+1] == ' '):
                                                dontAdd = False
                                        elif (line[char] == "'") and (dontAdd == False):
                                                break
                                        if (dontAdd == False):
                                                if (line[char] is not '['):
                                                        if (line[char] is not ']'):
                                                                if (line[char-1] is not ']'):
                                                                        error = error + line[char]
                        else:
                                print("how could this possibly ever happen based on the initial command executed grepping by error or severe")
                                
                        #if error is not in the list of errors
                        if error not in errorList:
                                errorList.append(error)
                                numberOfErrors = numberOfErrors + 1
                                print "========================================================================================="
                                print "Error: " + str(numberOfErrors)
                                print ""
                                print "Error Description: " + str(error)
                                print ""
                                print "Full Error: "
                                print line
                                grepNumber = "grep -a --text '" + error + "' " + filePath + " | wc -l"
                                grepTime = "grep -a --text '" + error + "' " + filePath + " | awk '{print $1}'"
                                number = subprocess.Popen(grepNumber,shell=True,stdout=subprocess.PIPE,stderr=subprocess.STDOUT)
                                time = subprocess.Popen(grepTime,shell=True,stdout=subprocess.PIPE,stderr=subprocess.STDOUT)
                                output2 = number.communicate()[0]
                                output3 = time.communicate()[0]
                                print ""
                                print "Occurrences: " + output2
                                print "Time: "
                                print  output3
                                print "========================================================================================="

                return

        LogAnalyze();