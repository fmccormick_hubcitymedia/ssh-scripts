#!/usr/bin/python
# Author - Carl Keven Chang
# August 6, 2014
# Hub City Media

import sys
import subprocess
import shlex

filePath = sys.argv[1]

class OHSLogAnalyzer:

        #Analyze OHS access log
        def OHSAnalyze():

                #Get total request
                def OHSTotalRequests():
                        totalRequestCheck = "awk '$9 > 0' " + filePath + "|wc -l"
                        totalRequest= subprocess.Popen(totalRequestCheck,shell=True,stdout=subprocess.PIPE,stderr=subprocess.STDOUT)
                        outputTotalRequest = totalRequest.communicate()[0]
                        return outputTotalRequest

                #Get total errors
                def OHSTotalErrors():
                        totalErrorCheck = "awk '$9==400' " + filePath + "|wc -l"
                        totalError = subprocess.Popen(totalErrorCheck,shell=True,stdout=subprocess.PIPE,stderr=subprocess.STDOUT)
                        outputTotalError = totalError.communicate()[0]
                        return outputTotalError

                #Get occurrence of error
                def OHSErrorCount(resource):
                        occurCheck = "awk '$9==400' " + filePath + " | grep '" + resource + "' | wc -l"
                        occurrence = subprocess.Popen(occurCheck,shell=True,stdout=subprocess.PIPE,stderr=subprocess.STDOUT)
                        outputOccur = occurrence.communicate()[0]
                        return outputOccur

                #Get resource request count
                def OHSRequestCount(resource):
                        requestCheck = "grep '" + resource + "' " + filePath + "| wc -l"
                        requests = subprocess.Popen(requestCheck,shell=True,stdout=subprocess.PIPE,stderr=subprocess.STDOUT)
                        outputRequests = requests.communicate()[0]
                        return outputRequests

                #Get timestamps of all errors
                def OHSTimeStamp(resource):
                        timeCheck = "awk '$9==400' " + filePath + " | grep '" + resource + "' | awk '{print $4}'"
                        timeStamps = subprocess.Popen(timeCheck,shell=True,stdout=subprocess.PIPE,stderr=subprocess.STDOUT)
                        outputTimeStamps = timeStamps.communicate()[0]
                        return outputTimeStamps

                #Getting total error percentage
                print "========================================================================================="
                outputTotalError = OHSTotalErrors()
                outputTotalRequest = OHSTotalRequests()
                print "Total Requests: " + str(outputTotalRequest)
                print "Total Errors: " + str(outputTotalError)
                percentageTotal = 100 * int(outputTotalError) / int(outputTotalRequest)
                print "Percentage of Errors Total: " + str(percentageTotal) + "%"
                print "========================================================================================="

                resourceList = ['auth_cred_submit','obrareq.cgi','samlogout','businessServices.jspx','gotoredirect','VGPortal/afr']
                #requestList = []
                numberOfErrors = 1

                #Greping each resource
                for resource in resourceList:

                        print "REPORTING " + resource + " ERRORS"
                        print "========================================================================================="
                        outputOccur = OHSErrorCount(resource)
                        print "Occurences: " + outputOccur

                        outputRequests = OHSRequestCount(resource)
                        print "Total Requests: " + outputRequests

                        percentage = 0;
                        try:
                                percentage = 100 * int(outputOccur) / int(outputRequests)
                        except ZeroDivisionError:
                                percentage = 0;
                        print "Percentage of Errors: " + str(percentage) + "%"
                        print "Times:"
                        print OHSTimeStamp(resource)
                        errorFound = True
                        print "========================================================================================="

                        if outputOccur == 0:
                                print "No Errors Found"

                print "========================================================================================="
                print "REPORTING OTHER ERRORS"
                otherErrorCmd = "awk '$9==400' " + filePath + "| grep -v 'auth_cred_submit\|obrareq.cgi\|samlogout\|businessServices.jspx\|gotoredirect\|VGPortal/afr'"
                otherErrorCheck = subprocess.Popen(otherErrorCmd,shell=True,stdout=subprocess.PIPE,stderr=subprocess.STDOUT)
                outputOther = otherErrorCheck.communicate()[0]
                otherList = []

                #Analyze request
                for line in outputOther.splitlines():
                        lineLog = shlex.split(line);
                        resrc = lineLog[5].split()
                        src = resrc[1].split("?")
                        if src[0] not in otherList:
                                print "========================================================================================="
                                otherList.append(src[0])
                                print "Error #" + str(numberOfErrors)
                                numberOfErrors = numberOfErrors+ 1
                                print "Error: " + src[0]
                                outputOccur = OHSErrorCount(src[0])
                                print "Occurences: " + outputOccur

                                outputRequests = OHSRequestCount(src[0])
                                print "Total Requests: " + outputRequests

                                percentage = 0;
                                try:
                                        percentage = 100 * int(outputOccur) / int(outputRequests)
                                except ZeroDivisionError:
                                        percentage = 0;
                                print "Percentage of Errors: " + str(percentage) + "%"
                                print "Times:"
                                print OHSTimeStamp(src[0])

                if not otherList:
                        print "========================================================================================="
                        print "No Other Errors"

                print "========================================================================================="
                return

        OHSAnalyze();
