#!/usr/bin/python
# Author - Carl Keven Chang
# August 15, 2014
# Hub City Media

import sys
import subprocess
import shlex

filePath = sys.argv[1]

class logAnalyzer:

        #Analyze OIM diagnostic log
        def LogAnalyze():

                #Grep for errors
                grepErrors = "grep -a --text 'ERROR\|SEVERE' " + filePath
                errors = subprocess.Popen(grepErrors,shell=True,stdout=subprocess.PIPE,stderr=subprocess.STDOUT)
                output1 = errors.communicate()[0]

                dontAdd = False
                errorList = []
                numberOfErrors = 1
                outputMsgs = []

                #Parsing error
                for line in output1.splitlines():
                        dontAdd = False
                        error = ""
                        if ('ERROR' in line) or ('SEVERE' in line):

                                for char in range(len(line)):
                                        if (line[char] == '['):
                                                dontAdd = True
                                        elif (line[char] == ']') and (char < (len(line)-1)):
						if (line[char+1] == ' '): 
                                                	dontAdd = False
                                        elif (line[char] == "'") and (dontAdd == False):
                                                break
                                        if (dontAdd == False):
                                                if (line[char] is not '['):
                                                        if (line[char] is not ']'):
                                                                if (line[char-1] is not ']'):
                                                                        error = error + line[char]
                        #if error is not in the list of errors
                        if error not in errorList:
                                errorList.append(error)
                                print "========================================================================================="
                                print "Error: " + str(numberOfErrors)
                                print ""
                                print "Error Description: " + str(error)
                                print ""
                                print "Full Error: "
                                print line
                                numberOfErrors = numberOfErrors + 1
                                grepNumber = "grep -a --text '" + error + "' " + filePath + " | wc -l"
                                grepTime = "grep -a --text '" + error + "' " + filePath + " | awk '{print $1}'"
                                number = subprocess.Popen(grepNumber,shell=True,stdout=subprocess.PIPE,stderr=subprocess.STDOUT)
                                time = subprocess.Popen(grepTime,shell=True,stdout=subprocess.PIPE,stderr=subprocess.STDOUT)
                                output2 = number.communicate()[0]
                                output3 = time.communicate()[0]
                                print ""
                                print "Occurrences: " + output2
                                print "Time: "
                                print  output3
                                print "========================================================================================="

                return

        LogAnalyze();